= Relatório de Revisão da Monografia
Juno Takano \
15/01/2024 

== Contextualização
Este relatório refere-se à revisão da monografia, feita após a integração dos três documentos desenvolvidos ao longo do segundo semestre de 2023.

== Processo
O trabalho consistiu principalmente na melhora da coesão entre as seções e na remoção de material duplicado. Algumas seções foram expandidas.

A listagem de requisitos foi expandida através da leitura, identificando-se fatores que serão importantes para os testes com turmas, como exercícios de ordenação e funcionalidade de contas e controles de acesso.

Para cumprir a nova listagem de requisitos e alinhar o desenvolvimento à proposta, seria interessante priorizar:
- Autenticação por conta
- Níveis de acesso
- Testes automatizados
- Questões de ordenação
- Revisar a bibliografia sobre criação de exercícios
- Criação de conjuntos de exercícios para os primeiros testes
- Estudo dos métodos de avaliação
  - Elaboração dos formulários de avaliação
  - Elaboração das entrevistas de avaliação

Foi possível identificar também algumas tarefas ainda por finalizar referentes ao texto da monografia: 
- O resumo precisa ser reduzido.
- Há trechos que carecem de referências (anotados com comentários)
  - Corrigir os diagramas de sequência
- Há trechos que carecem de melhor coesão
- Confirmar: A primeira referência "Schaler, 2021" na revisão bibliográfica era "Strauss, 2021" em um trecho duplicado
- Realizar a leitura de "How Committees Invent"

Outras tarefas de desenvolvimento que foram levantadas durante esta revisão mas que devem ficar para versões posteriores ao protótipo (v0.2.0):
- Melhorar estruturação dos eventos
  - Incluir o ator "Docente" no diagrama de casos de uso
  - Armazenar eventos em JSON com um campo para a versão da API
