= Relatório de Integração de Documentos
Juno Takano \
09/01/2023 

== Contextualização
Este relatório refere-se à integração de três documentos escritos durante o segundo semestre de 2023:

- Relatório de desenvolvimento do protótipo para a disciplina de Engenharia de Software Aplicada
- Trabalho final para a disciplina de Metodologia da Pesquisa Científica e Tecnológica
- Documento pré-existente da monografia do TCC

== Processo
Os documentos foram gerados utilizando diferentes ferramentas e seguindo diferentes propostas pedagógicas.

Para a disciplina de Engenharia de Software Aplicada, o foco estava na fidelidade da relação projeto-implementação, alinhando código escrito à documentação associada. Este texto trazia uma linguagem mais técnica e detalhada a respeito do próprio código da aplicação.

O trabalho realizado para a disciplina de Metodologia da Pesquisa Científica e Tecnológica enfatizava a estrutura própria de um TCC, formatação de referências, estilo de escrita, entre outros. 

A proposta do trabalho incluía a inserção de fatos fictícios que emulassem os possíveis resultados e acontecimentos futuros que se referem às fases mais avançadas do projeto. A maioria destas previsões foram retiradas do texto final após a integração.

Enquanto estes dois utilizaram a ferramenta `Typst`, o texto anterior do TCC, escrito em `LaTeX`, era mais estritamente referenciado, ainda com pouco conteúdo técnico e referente apenas à introdução e revisão bibliográfica.

A junção dos três trouxe maior substância ao documento, mas também apresentou desafios na tradução do código de uma linguagem para outra. A ferramenta `Typst` oferece vantagens como uma sintaxe mais limpa, documentação mais concisa e organizada, enquanto o `LaTeX` apresenta um vasto ecossistema de bibliotecas, documentação extensiva porém de difícil consulta, e uma sintaxe de código mais poluída.

No mais, os diferentes estilos de escrita e propostas tornaram a integração um processo mais focado da deduplicação de informações, restando ainda como tarefa pendente uma releitura que adeque melhor o texto para seguir um estilo e estrutura mais uniformes. 
